# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Onap_so System. The API that was used to build the adapter for Onap_so is usually available in the report directory of this adapter. The adapter utilizes the Onap_so API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The ONAP Service Orchestrator adapter from Itential is used to integrate the Itential Automation Platform (IAP) with ONAP Service Orchestrator. 

With this adapter you have the ability to perform operations with ONAP Service Orchestrator such as:

- Service Instances
- Service Instantiation
- Node Health Check

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 

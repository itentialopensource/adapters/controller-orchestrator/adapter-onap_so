## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for ONAP Service Orchestrator. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for ONAP Service Orchestrator.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the ONAP Service Orchestration. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">createE2EServiceInstance(body, version, callback)</td>
    <td style="padding:15px">Create an E2E Service Instance on a version provided</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/e2eServiceInstances/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateE2EServiceInstance(body, version, serviceId, callback)</td>
    <td style="padding:15px">Update an E2E Service Instance on a version provided and serviceId</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/e2eServiceInstances/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteE2EServiceInstance(body, version, serviceId, callback)</td>
    <td style="padding:15px">Delete E2E Service Instance on a specified version and serviceId</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/e2eServiceInstances/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getE2EServiceInstances(serviceId, version, operationId, callback)</td>
    <td style="padding:15px">Find e2eServiceInstances Requests for a given serviceId and operationId</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/e2eServiceInstances/{pathv1}/{pathv2}/operations/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">scaleE2EServiceInstance(body, version, serviceId, callback)</td>
    <td style="padding:15px">Scale E2E Service Instance on a specified version</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/e2eServiceInstances/{pathv1}/{pathv2}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">compareModelwithTargetVersion(body, serviceId, version, callback)</td>
    <td style="padding:15px">Find added and deleted resources of target model for the e2eserviceInstance on a given serviceId</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/e2eServiceInstances/{pathv1}/{pathv2}/modeldifferences?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">globalHealthcheck(enableBpmn, callback)</td>
    <td style="padding:15px">Performing global health check</td>
    <td style="padding:15px">{base_path}/{version}/globalhealthcheck?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">nodeHealthcheck(callback)</td>
    <td style="padding:15px">Performing node health check</td>
    <td style="padding:15px">{base_path}/{version}/nodehealthcheck?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrchestrationRequest(version, callback)</td>
    <td style="padding:15px">Find Orchestrated Requests for a URI Information</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/orchestrationRequests/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrchestrationRequestForReqId(requestId, version, callback)</td>
    <td style="padding:15px">Find Orchestrated Requests for a given requestId</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/orchestrationRequests/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unlockOrchestrationRequest(body, requestId, version, callback)</td>
    <td style="padding:15px">Unlock Orchestrated Requests for a given requestId</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/orchestrationRequests/{pathv1}/{pathv2}/unlock?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deactivateAndCloudDeleteVfModuleInstance(body, version, serviceInstanceId, vnfInstanceId, vfmoduleInstanceId, callback)</td>
    <td style="padding:15px">Deactivate and Cloud Delete VfModule instance</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/serviceInstantiation/{pathv1}/serviceInstances/{pathv2}/vnfs/{pathv3}/vfModules/{pathv4}/deactivateAndCloudDelete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enablePort(body, version, serviceInstanceId, configurationInstanceId, callback)</td>
    <td style="padding:15px">Enable Port Mirroring</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/serviceInstantiation/{pathv1}/serviceInstances/{pathv2}/configurations/{pathv3}/enablePort?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disablePort(body, version, serviceInstanceId, configurationInstanceId, callback)</td>
    <td style="padding:15px">Disable Port Mirroring</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/serviceInstantiation/{pathv1}/serviceInstances/{pathv2}/configurations/{pathv3}/disablePort?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activatePort(body, version, serviceInstanceId, configurationInstanceId, callback)</td>
    <td style="padding:15px">Activate Port Mirroring</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/serviceInstantiation/{pathv1}/serviceInstances/{pathv2}/configurations/{pathv3}/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deactivatePort(body, version, serviceInstanceId, configurationInstanceId, callback)</td>
    <td style="padding:15px">Deactivate Port Mirroring</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/serviceInstantiation/{pathv1}/serviceInstances/{pathv2}/configurations/{pathv3}/deactivate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addRelationships(body, version, serviceInstanceId, callback)</td>
    <td style="padding:15px">Add Relationships to a Service Instance</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/serviceInstantiation/{pathv1}/serviceInstances/{pathv2}/addRelationships?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">scaleOutVfModule(body, version, serviceInstanceId, vnfInstanceId, callback)</td>
    <td style="padding:15px">VF Auto Scale Out</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/serviceInstantiation/{pathv1}/serviceInstances/{pathv2}/vnfs/{pathv3}/vfModules/scaleOut?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">inPlaceSoftwareUpdate(body, version, serviceInstanceId, vnfInstanceId, callback)</td>
    <td style="padding:15px">Perform VNF software update</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/serviceInstantiation/{pathv1}/serviceInstances/{pathv2}/vnfs/{pathv3}/inPlaceSoftwareUpdate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applyUpdatedConfig(body, version, serviceInstanceId, vnfInstanceId, callback)</td>
    <td style="padding:15px">Apply updated configuration</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/serviceInstantiation/{pathv1}/serviceInstances/{pathv2}/vnfs/{pathv3}/applyUpdatedConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createServiceInstance(body, version, callback)</td>
    <td style="padding:15px">Create a Service Instance on a version provided</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/serviceInstantiation/{pathv1}/serviceInstances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activateServiceInstance(body, version, serviceInstanceId, callback)</td>
    <td style="padding:15px">Activate provided Service Instance</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/serviceInstantiation/{pathv1}/serviceInstances/{pathv2}/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deactivateServiceInstance(body, version, serviceInstanceId, callback)</td>
    <td style="padding:15px">Deactivate provided Service Instance</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/serviceInstantiation/{pathv1}/serviceInstances/{pathv2}/deactivate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteServiceInstance(body, version, serviceInstanceId, callback)</td>
    <td style="padding:15px">Delete provided Service Instance</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/serviceInstantiation/{pathv1}/serviceInstances/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assignServiceInstance(body, version, callback)</td>
    <td style="padding:15px">Assign Service Instance</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/serviceInstantiation/{pathv1}/serviceInstances/assign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unassignServiceInstance(body, version, serviceInstanceId, callback)</td>
    <td style="padding:15px">Unassign Service Instance</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/serviceInstantiation/{pathv1}/serviceInstances/{pathv2}/unassign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPortConfiguration(body, version, serviceInstanceId, callback)</td>
    <td style="padding:15px">Create Port Mirroring Configuration</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/serviceInstantiation/{pathv1}/serviceInstances/{pathv2}/configurations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePortConfiguration(body, version, serviceInstanceId, configurationInstanceId, callback)</td>
    <td style="padding:15px">Delete provided Port</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/serviceInstantiation/{pathv1}/serviceInstances/{pathv2}/configurations/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeRelationships(body, version, serviceInstanceId, callback)</td>
    <td style="padding:15px">Remove Relationships from Service Instance</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/serviceInstantiation/{pathv1}/serviceInstances/{pathv2}/removeRelationships?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createVnfInstance(body, version, serviceInstanceId, callback)</td>
    <td style="padding:15px">Create VNF on a specified version and serviceInstance</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/serviceInstantiation/{pathv1}/serviceInstances/{pathv2}/vnfs?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceVnfInstance(body, version, serviceInstanceId, vnfInstanceId, callback)</td>
    <td style="padding:15px">Replace provided VNF instance</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/serviceInstantiation/{pathv1}/serviceInstances/{pathv2}/vnfs/{pathv3}/replace?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateVnfInstance(body, version, serviceInstanceId, vnfInstanceId, callback)</td>
    <td style="padding:15px">Update VNF on a specified version, serviceInstance and vnfInstance</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/serviceInstantiation/{pathv1}/serviceInstances/{pathv2}/vnfs/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVnfInstance(body, version, serviceInstanceId, vnfInstanceId, callback)</td>
    <td style="padding:15px">Delete provided VNF instance</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/serviceInstantiation/{pathv1}/serviceInstances/{pathv2}/vnfs/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createVfModuleInstance(body, version, serviceInstanceId, vnfInstanceId, callback)</td>
    <td style="padding:15px">Create VfModule on a specified version, serviceInstance and vnfInstance</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/serviceInstantiation/{pathv1}/serviceInstances/{pathv2}/vnfs/{pathv3}/vfModules?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">replaceVfModuleInstance(body, version, serviceInstanceId, vnfInstanceId, vfmoduleInstanceId, callback)</td>
    <td style="padding:15px">Create VfModule on a specified version, serviceInstance and vnfInstance</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/serviceInstantiation/{pathv1}/serviceInstances/{pathv2}/vnfs/{pathv3}/vfModules/{pathv4}/replace?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateVfModuleInstance(body, version, serviceInstanceId, vnfInstanceId, vfmoduleInstanceId, callback)</td>
    <td style="padding:15px">Update VfModule on a specified version, serviceInstance, vnfInstance and vfModule</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/serviceInstantiation/{pathv1}/serviceInstances/{pathv2}/vnfs/{pathv3}/vfModules/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVfModuleInstance(body, version, serviceInstanceId, vnfInstanceId, vfmoduleInstanceId, callback)</td>
    <td style="padding:15px">Delete provided VfModule instance</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/serviceInstantiation/{pathv1}/serviceInstances/{pathv2}/vnfs/{pathv3}/vfModules/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createVolumeGroupInstance(body, version, serviceInstanceId, vnfInstanceId, callback)</td>
    <td style="padding:15px">Create VolumeGroup on a specified version, serviceInstance, vnfInstance</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/serviceInstantiation/{pathv1}/serviceInstances/{pathv2}/vnfs/{pathv3}/volumeGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateVolumeGroupInstance(body, version, serviceInstanceId, vnfInstanceId, volumeGroupInstanceId, callback)</td>
    <td style="padding:15px">Update VolumeGroup on a specified version, serviceInstance, vnfInstance and volumeGroup</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/serviceInstantiation/{pathv1}/serviceInstances/{pathv2}/vnfs/{pathv3}/volumeGroups/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVolumeGroupInstance(body, version, serviceInstanceId, vnfInstanceId, volumeGroupInstanceId, callback)</td>
    <td style="padding:15px">Delete provided VolumeGroup instance</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/serviceInstantiation/{pathv1}/serviceInstances/{pathv2}/vnfs/{pathv3}/volumeGroups/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createNetworkInstance(body, version, serviceInstanceId, callback)</td>
    <td style="padding:15px">Create NetworkInstance on a specified version and serviceInstance</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/serviceInstantiation/{pathv1}/serviceInstances/{pathv2}/networks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateNetworkInstance(body, version, serviceInstanceId, networkInstanceId, callback)</td>
    <td style="padding:15px">Update VolumeGroup on a specified version, serviceInstance, networkInstance</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/serviceInstantiation/{pathv1}/serviceInstances/{pathv2}/networks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNetworkInstance(body, version, serviceInstanceId, networkInstanceId, callback)</td>
    <td style="padding:15px">Delete provided Network instance</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/serviceInstantiation/{pathv1}/serviceInstances/{pathv2}/networks/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">queryFilters(taskId, originalRequestId, subscriptionServiceType, nfRole, buildingBlockName, originalRequestDate, originalRequestorId, version, callback)</td>
    <td style="padding:15px">Finds Manual Tasks</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/tasks/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOperationEnvironment(body, version, callback)</td>
    <td style="padding:15px">Create an Operational Environment</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/cloudResources/{pathv1}/operationalEnvironments?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">activateOperationEnvironment(body, version, operationalEnvironmentId, callback)</td>
    <td style="padding:15px">Activate an Operational Environment</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/cloudResources/{pathv1}/operationalEnvironments/{pathv2}/activate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deactivateOperationEnvironment(body, version, operationalEnvironmentId, callback)</td>
    <td style="padding:15px">Deactivate an Operational Environment</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/cloudResources/{pathv1}/operationalEnvironments/{pathv2}/deactivate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">unlockOrchestrationRequestForReqId(body, requestId, version, callback)</td>
    <td style="padding:15px">Unlock CloudOrchestration requests for a specified requestId</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/cloudResourcesRequests/{pathv1}/{pathv2}/unlock?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOperationEnvironmentStatusFilter(version, callback)</td>
    <td style="padding:15px">Get status of an Operational Environment based on filter criteria</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/cloudResourcesRequests/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateModelDistributionStatus(body, version, distributionId, callback)</td>
    <td style="padding:15px">Update model distribution status</td>
    <td style="padding:15px">{base_path}/{version}/onap/so/infra/modelDistributions/{pathv1}/distributions/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>

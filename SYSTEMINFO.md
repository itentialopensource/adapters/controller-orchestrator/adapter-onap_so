# ONAP Service Orchestrator

Vendor: ONAP
Homepage: https://www.onap.org/

Product: Service Orchestrator
Product Page: https://wiki.onap.org/pages/viewpage.action?pageId=4719246

## Introduction
We classify ONAP Service Orchestrator into the Network Services domain as ONAP Service Orchestrator facilitates the orchestration of network functions, enabling the provisioning, configuration, and optimization of network resources such as VNFs. We also classify ONAP Service Orchestrator into the Data Center domain due to its capabilities to manage and orchestrate data center resources effectively.

"The SO provides the highest level of service orchestration in the ONAP architecture"

## Why Integrate
The ONAP Service Orchestrator adapter from Itential is used to integrate the Itential Automation Platform (IAP) with ONAP Service Orchestrator. 

With this adapter you have the ability to perform operations with ONAP Service Orchestrator such as:

- Service Instances
- Service Instantiation
- Node Health Check

## Additional Product Documentation
The [API documents for ONAP Service Orchestrator](https://docs.onap.org/projects/onap-so/en/latest/api/offered_consumed_apis.html#offeredapis)